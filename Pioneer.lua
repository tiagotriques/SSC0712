-- Setup
if (sim_call_type == sim.syscb_init) then
	usensors={-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}
	for i=1,16,1 do
		usensors[i]=sim.getObjectHandle("Pioneer_p3dx_ultrasonicSensor"..i)
	end
	motorLeft  = sim.getObjectHandle("Pioneer_p3dx_leftMotor")
	motorRight = sim.getObjectHandle("Pioneer_p3dx_rightMotor")
	noDetectionDist=0.5
	maxDetectionDist=0.2
	detect={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
	braitenbergL={-0.2,-0.4,-0.6,-0.8,-1,-1.2,-1.4,-1.6, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}
	braitenbergR={-1.6,-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2, 0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}
	v0=4
	last_dist = 0
	fim = false

	communicationTube=sim.tubeOpen(1,'2D_SCANNER_DATA',1)

	-- Linha de referência
	linha_alvo = sim.addDrawingObject(sim.drawing_lines, 2, 0, -1, 9999, {1,1,0})

	-- Tabela de alvos hardcoded
	target_table = {}
	table.insert(target_table, { -2.0040, -8.1370})
	table.insert(target_table, {-10.1040, -4.3870})
	table.insert(target_table, {-10.1290,  2.9380})
	table.insert(target_table, { -2.1540,  3.0880})
	table.insert(target_table, { -6.1540, -2.4870})
	table.insert(target_table, {  1.2710, -7.3370})
	table.insert(target_table, {  7.6710, -7.8620})
	table.insert(target_table, { -0.2290,  2.9630})
	table.insert(target_table, { -1.2290, 10.6630})
	current_target = 9
end

if (sim_call_type == sim.syscb_cleanup) then

end

if (sim_call_type == sim.syscb_actuation) then
	-- Braitenberg
	for i=1,16,1 do
		res, dist = sim.readProximitySensor(usensors[i])
		if (res > 0) and (dist < noDetectionDist) then
			if (dist < maxDetectionDist) then
				dist = maxDetectionDist
			end
			detect[i] = 1-((dist-maxDetectionDist)/(noDetectionDist-maxDetectionDist))
		else
			detect[i] = 0
		end
	end

	obstacle = false
	for i=1,16,1 do
		if detect[i] > 0 then
			obstacle = true
		end
	end

	vLeft=v0
	vRight=v0

	for i=1,16,1 do
		vLeft  = vLeft  + braitenbergL[i] * detect[i]
		vRight = vRight + braitenbergR[i] * detect[i]
	end

	-- Pegar posiçao do GPS do Pioneer
	posXF=sim.getFloatSignal('gpsPioneerFrenteX')
	posYF=sim.getFloatSignal('gpsPioneerFrenteY')

	-- Apagar linhas antigas
	sim.addDrawingObjectItem(linha_alvo, nil)

	-- Desenhar uma linha da posição do robô até o alvo atual
	local line1={target_table[current_target][1], target_table[current_target][2], 0.3, posXF, posYF, 0.1388}
	sim.addDrawingObjectItem(linha_alvo,line1)

	-- Se Pioneer está proximo do alvo, avançar para o próximo
	local erro = 0.2
	if between(posXF, target_table[current_target][1], erro) then
		if between(posYF, target_table[current_target][2], erro) then
			if current_target ~= 9 then
				current_target = current_target + 1
			else
				fim = true
			end
		end
	end

	-- Chegou no ultimo ponto
	if fim == true then
		vRight = 0
		vLeft = 0
	end

	-- Ângulos de -180º a 180º
	local dx = target_table[current_target][1] - posXF
	local dy = target_table[current_target][2] - posYF
	local theta = math.deg(math.atan2(dy,dx))
	local pioneer_ori = math.deg(sim.getObjectOrientation(sim.getObjectHandle("Pioneer_p3dx"), -1)[3])

	-- Se chegou no fim, motores parados
	if fim == true then
		sim.setJointTargetVelocity(motorLeft, 0)
		sim.setJointTargetVelocity(motorRight, 0)
	end

	-- Se linha alvo e Pioneer 'alinhadas'
	if between(pioneer_ori, theta, 8) then
		aligned = true
	else
		aligned = false
	end

	-- Se tiverem obstáculos, braitenberg FTW
	if obstacle == true then
		sim.setJointTargetVelocity(motorLeft,  vLeft)
		sim.setJointTargetVelocity(motorRight, vRight)
	else
		-- Caso contrário
		-- Se alinhado, ande
		if aligned == true then
			sim.setJointTargetVelocity(motorLeft,  v0)
			sim.setJointTargetVelocity(motorRight, v0)
		else
			-- Ou gire
			turn_speed = 1.0
			-- Ver qual lado é melhor de girar
			if theta < pioneer_ori then
				sim.setJointTargetVelocity(motorLeft, turn_speed)
				sim.setJointTargetVelocity(motorRight, -turn_speed)
			else
				sim.setJointTargetVelocity(motorLeft, -turn_speed)
				sim.setJointTargetVelocity(motorRight, turn_speed)
			end
		end
	end
end

-- val - diff < x < val + diff
function between(x, val, diff)
	if ((val - diff < x) and (x < val + diff)) then
		return true
	else
		return false
	end
end

